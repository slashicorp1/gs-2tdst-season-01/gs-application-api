#!/bin/bash
cat <<EOF >> manifest.yml
---
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: default
  name: ${CI_PROJECT_NAME}
  labels:
    app: ${CI_PROJECT_NAME}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: ${CI_PROJECT_NAME}
  template:
    metadata:
      labels:
        app: ${CI_PROJECT_NAME}
    spec:
      containers:
      - name: ${CI_PROJECT_NAME}
        image: ${CI_REGISTRY}/${CI_PROJECT_PATH}:${CI_COMMIT_SHORT_SHA}
        ports:
        - containerPort: ${APP_CONTAINER_PORT}
        env:
        - name: DATABASE_ENDPOINT
          value: ${DATABASE_ENDPOINT}
        - name: DB_PORT
          value: ${DB_PORT}
        - name: DB_USER
          value: ${DB_USER}
        - name: DB_PASS
          value: ${DB_PASS}
        - name: DB_NAME
          value: ${DB_NAME}
      imagePullSecrets:
        - name: ${CI_PROJECT_NAME}
---
apiVersion: v1
kind: Service
metadata:
  namespace: default
  name: ${CI_PROJECT_NAME}-svc
  labels:
    app: ${CI_PROJECT_NAME}
spec:
  ports:
  - port: 80
    name: http
    targetPort: ${APP_CONTAINER_PORT}
  - port: 443
    targetPort: ${APP_CONTAINER_PORT}
    name: https
  selector:
    app: ${CI_PROJECT_NAME}
---
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  namespace: default
  name: ${CI_PROJECT_NAME}-ingress
  annotations:
    kubernetes.io/tls-acme: "true"
    kubernetes.io/ingress.class: "nginx"
    nginx.ingress.kubernetes.io/rewrite-target: /setups/$2
spec:
  rules:
    - host: ${CI_PROJECT_NAME}.slashicorp.com.br
      http:
        paths:
          - path: /setups(/|$)(.*)
            backend:
              serviceName: ${CI_PROJECT_NAME}-svc
              servicePort: 80
---
EOF
